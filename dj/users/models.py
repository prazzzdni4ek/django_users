import datetime

from django.db import models
from django.utils import timezone

# Create your models here.
class User_info(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    patronymic = models.CharField(max_length=50)
    birthday = models.DateField('birthday')
    city = models.ForeignKey('Cityes', on_delete=models.CASCADE)

    def __str__(self):
        full_name = [self.last_name, self.first_name, self.patronymic]
        return ' '.join(full_name)

class Cityes(models.Model):
    city = models.CharField(max_length=50)
    def __str__(self):
        return self.city
