from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from .models import User_info

def index(request):
    latest_users_list = User_info.objects.all()
    template = loader.get_template('users/index.html')
    context = { 'latest_users_list': latest_users_list }
    return HttpResponse(template.render(context, request))

def detail(request, id):
    try:
        user = User_info.objects.get(pk=id)
    except User_info.DoesNotExist:
        raise Http404("User does not exist")
    return render(request, 'users/detail.html', {'user': user})
