import psycopg, sys, configparser

config = configparser.ConfigParser()
config.read("./variables.conf")
dbname = config.get("base", "dbname")
user = config.get("base", "user")
password = config.get("base", "password")
host = config.get("base", "host")

class Users(object):

    def __init__(self, func):
        self.func = func
      
        if func == "check":
            self.check()
        elif func == "insert":
            name = input("enter name:")
            date = input("enter date(YYYY-MM-DD):")
            city = input("enter city:")
            self.insert(name, date, city)
        else: 
            print("usage: users.py [COMMAND]\nCommands:\ncheck - select all rows from table\ninsert - insert in db")

    def check(self):
        connect = psycopg.connect(dbname=dbname, user=user, password=password, host=host)
        cursor = connect.cursor()
        cursor.execute("SELECT * FROM users;")
        print(cursor.fetchall())
        cursor.close()
        connect.close()
    
    def insert(self, name, date, city):
        connect = psycopg.connect(dbname=dbname, user=user, password=password, host=host)
        cursor = connect.cursor()
        cursor.execute(f"INSERT INTO users (name, date, city)  VALUES ('{name}', '{date}', '{city}');")
        connect.commit()
        cursor.close()
        connect.close()


if __name__ == "__main__":
    user = Users(sys.argv[1])
